## Setup and install

To set up and install the code do:
```
mkdir ABExample && cd ABExample
git clone ssh://git@gitlab.cern.ch:7999/sargyrop/abexample.git source
cd source
source setup.sh
source install.sh
```

## Every time you log in

```
source setup.sh
```

## Run code

```
cd run
runxAODAnalysis
```
