#include "EventLoop/Job.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "xAODRootAccess/Init.h"

#include "ABExample/xAODAnalysis.h"

#include "EventLoop/DirectDriver.h"
#include "AnaAlgorithm/AnaAlgorithmConfig.h"

#include "SampleHandler/Sample.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"

#include <iostream>
#include <TSystem.h>

int main(int argc, char *argv[]) {
  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  
  if (argc > 1) submitDir = argv[1];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create the sample handler
  SH::SampleHandler sh;
  std::cout << "Scanning samples:" << std::endl;
  const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/s/sargyrop/public/FTAG/");
  SH::ScanDir().filePattern("user.thuffman.24280485.EXT0._000219.root").scan(sh,inputFilePath);

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString("nc_tree", "CollectionTree");

  // Print what we found:
  std::cout << "Printing sample handler contents:" << std::endl;
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler(sh);
  job.options()->setDouble (EL::Job::optMaxEvents, 500);
  job.options()->setString( EL::Job::optSubmitDirMode, "unique-link");
  
  // create algorithm, set job options, manipulate members and add our analysis
  // to the job:
  EL::AnaAlgorithmConfig alg;
  alg.setType ("xAODAnalysis");
  
  // set the name of the algorithm (this is the name use with
  // messages)
  alg.setName ("xAODAnalysis");
  
  // Change message output level
  //alg.setProperty ("OutputLevel", MSG::DEBUG);
  
  // add algorithm to job
  job.algsAdd(alg);

  EL::DirectDriver driver;
  driver.submit(job, submitDir);

  return 0;
}
