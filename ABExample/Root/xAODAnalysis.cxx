#include <AsgTools/MessageCheck.h>
#include <ABExample/xAODAnalysis.h>

// Stuff from ROOT
#include <TH1.h>

// Stuff we need from athena
#include <xAODEventInfo/EventInfo.h>


xAODAnalysis::xAODAnalysis(const std::string& name, ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
    
}


StatusCode xAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  ANA_MSG_DEBUG("Initialise");

  ANA_CHECK( book( TH1F("h_jet1_pt",          "h_jet1_pt",         100,   0,  5000) ));
  ANA_CHECK( book( TH1F("h_N_pixHits",        "h_N_pixHits",       100,   0,  2000) ));
  ANA_CHECK( book( TH1F("h_pix_assoc_Njets",  "h_pix_assoc_Njets",  10,   0,    10) ));
  ANA_CHECK( book( TH1F("h_dRjets_sharedPix", "h_dRjets_sharedPix", 20,   0,     2) ));
  ANA_CHECK( book( TH1F("h_pixHit_globalX",   "h_pixHit_globalX",  460, -230,  230) ));
  ANA_CHECK( book( TH1F("h_pixHit_globalY",   "h_pixHit_globalY",  460, -230,  230) ));
  ANA_CHECK( book( TH1F("h_pixHit_globalZ",   "h_pixHit_globalZ", 1600, -800,  800) ));
  ANA_CHECK( book( TH1F("h_pixHit_R2D",       "h_pixHit_R2D",      200,    0,  200) ));
  ANA_CHECK( book( TH1F("h_pixHit_R3D",       "h_pixHit_R3D",      400,    0,  800) ));
  
  return StatusCode::SUCCESS;
}


StatusCode xAODAnalysis::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  ANA_MSG_DEBUG("Executing...");
  
  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

  // Event weight
  m_weight = eventInfo->mcEventWeight(0);

  // print out run and event number from retrieved object
  ANA_MSG_DEBUG("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber() << ", weight = " << m_weight);
    
  // Retrieve jet container
  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK(evtStore()->retrieve(jets, "AntiKt4EMPFlowJets"));
  if (jets->size() > 0) hist("h_jet1_pt")->Fill(jets->at(0)->pt()*0.001, m_weight);
  
  // Sanity check: check that jets are ordered in pt
  if (jets->size() > 1) {
    if (jets->at(0)->pt() < jets->at(1)->pt()) 
      ANA_MSG_ERROR("jet1 pt = " << jets->at(0)->pt() << " is smaller than jet 2 pt = " << jets->at(1)->pt());
  }
  
  // Retrieve hits container
  const xAOD::TrackMeasurementValidationContainer* pixelHits = nullptr;
  ANA_CHECK(evtStore()->retrieve(pixelHits, "PixelClusters"));
  
  // Find hits matching to jets
  matchHitsToJets(pixelHits, jets);
    
  // Fill pixel hit histograms
  hist("h_N_pixHits")->Fill(pixelHits->size(), m_weight);
  
  return StatusCode::SUCCESS;
}


StatusCode xAODAnalysis::matchHitsToJets(const xAOD::TrackMeasurementValidationContainer* pixelHits, 
					 const xAOD::JetContainer* jets) 
{  
  // Loop over the hits
  for (const xAOD::TrackMeasurementValidation *hit : *pixelHits) {
    // Get the hit global X,Y,Z position
    float x = hit->globalX();
    float y = hit->globalY();
    float z = hit->globalZ();
    
    hist("h_pixHit_globalX")->Fill(x, m_weight);
    hist("h_pixHit_globalY")->Fill(y, m_weight);
    hist("h_pixHit_globalZ")->Fill(z, m_weight);
    
    float r2d = std::sqrt(std::pow(x,2)+std::pow(y,2));
    float r3d = std::sqrt(std::pow(x,2)+std::pow(y,2)+std::pow(z,2));
    
    hist("h_pixHit_R2D")->Fill(r2d, m_weight);
    hist("h_pixHit_R3D")->Fill(r3d, m_weight);
    
    // Pseudorapidity and azimuthal angle of pixel
    float eta = (x*x+y*y > 0) ? std::atanh(z/(x*x+y*y)) : 0;
    float phi = (r3d > 0) ? std::acos(z/r3d) : 0;
    
    // Find if the pixel is associated to a jet
    bool pixelAssociatedToJet = false;
    int NjetsAssocPixel = 0;
    
    // Loop over the jets
    // For saving eta,phi of jets that have shared pixels
    float overlap_eta(0), overlap_phi(0);
    for (const xAOD::Jet *jet : *jets) {
      // Calculate dphi and deta of jet from pixel hit
      float deta = jet->eta() - eta;
      float dphi = jet->phi() - phi;
      float dR = std::sqrt(deta*deta + dphi*dphi);
      // Associate pixel to jet
      if (dR < 0.4) {
        if (pixelAssociatedToJet) { 	
	  ANA_MSG_INFO("This pixel is associated to more than one jet!");
	  float dR = std::sqrt(std::pow(overlap_eta - jet->eta(),2)+std::pow(overlap_phi - jet->phi(),2));
	  hist("h_dRjets_sharedPix")->Fill(dR, m_weight);
        }
	else pixelAssociatedToJet = true;
	++NjetsAssocPixel;
	overlap_eta = jet->eta();
	overlap_phi = jet->phi();
      }
    }
    hist("h_pix_assoc_Njets")->Fill(NjetsAssocPixel, m_weight);
    
  } // End loop over hits
  
  return StatusCode::SUCCESS;
}


StatusCode xAODAnalysis::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  
  ANA_MSG_INFO ("Finalise");
  
  return StatusCode::SUCCESS;
}
