#ifndef xAODAnalysis_H
#define xAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/TrackMeasurementValidationContainer.h>

class xAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  xAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

protected:
  StatusCode matchHitsToJets(const xAOD::TrackMeasurementValidationContainer* pixelHits, const xAOD::JetContainer* jets);

private:
  // Configuration, and any other types of variables go here.
  double m_weight;
  //TTree *m_myTree;
  //TH1 *m_myHist;
};

#endif
