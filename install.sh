#!/usr/bin/env bash

# Check that the script is sourced
if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
  echo "The script should be sourced"
  exit 1
fi

WKDIR=$PWD

# Go to build directory and make
cd ../build
cmake ../source
make
source x*/setup.sh

echo "All done. You can now go to the run/ directory and run: runxAODAnalysis"
