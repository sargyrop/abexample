#!/usr/bin/env bash

# Check that the script is sourced
if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
  echo "The script should be sourced"
  exit 1
fi

setupATLAS
asetup 21.2.160,AnalysisBase

# Check that the directory structure is as expected
WKDIR_NAME=$(basename $PWD)
if [[ "$WKDIR_NAME" != "source" ]] ; then
  echo "ERROR: you should download the package in a directory called source"
  return 
else 
  if [ ! -d ../run ] ; then mkdir ../run ; fi
  if [ ! -d ../build ] ; then 
    mkdir ../build ;
    echo "Package setup, now run: source install.sh"
    cd ../build 
    source x*/setup.sh
    cd $WKDIR_NAME
  fi 
fi
